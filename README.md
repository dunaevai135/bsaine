#BSA

[TOC]


#### registers:
acc - аккамулятор все вычисления тут

bac - неадресуемая память, меняеться с acc через swpb

bcc - неадресуемая память, меняеться с acc через swpc

spn - последний элемент стека

in - регистр (буфер) ввода, обращение к регистру являеться блокируещим, после считывания сбрасываеться

out - регистр вывода

#### instructions:
hlt - (halt program) need in end program

ldm - (load from memory) загружает из памяти/регистра данные в ACC

mov - из ACC в память/регистр/данные

push [dat/off/reg] - ложит в стек

pop - снимает со стека в ACC

popnil - удаляет последний элемент

jmp [lbl] (дальше аналогично) - безусловный переход

jez - переход если acc == 0

jnz - переход если acc != 0

jlz - переход если acc < 0

jgz - переход если acc > 0

call [lbl] - перехдит по метки сохраняя значения регистров и instuctionPointer'a в call-стеке

ret - востанавливает регистры переходит по сохранённому в call-стеке указателю

#### stack
стэк вызовов и пользователя дифиренцированы

call stack:

* ==[^previous call^]==
* instruction pointer
* registers:
* * ACC = 0b00000000
* * BCC = 0b00000001
* * BAC = 0b00000010
* * ERR = 0b00000011
* * IN  = 0b00000100
* * OUT = 0b00000101
* * SPN = 0b00000110
* ==[next call]==

#### интерпретатор
bsaINE - С++ (POSIX)

#### asembler
first section `[section __code]` - code(instructions) + data

second section `[section __stack]` - user stack

hlt - (halt program) need in end program

#### TODO
- [ ] приверить соответствие докам, по бинарнику