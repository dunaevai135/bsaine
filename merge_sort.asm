[section __code]
__start:
	;put from in to arr
		ldm #in
		push
		swpb
		pop
		swpb
		swpc
		ldm arr
		push
		swpc
	.loop:
		swpc
		ldm #in
		mov [#spn]
		
		pop
		add 0x2 ;because word
		push

		swpc
		sub 0x1
		jgz .loop
	
	swpb
	sub 1
	push ;right for print_arr
	push ;right for mergesort
	push 0
	call mergesort
	push 0
	call print_arr
	
	hlt

mergesort:;(stack: dat left, dat right)
	pop
	sub #spn
	jez .end
	add #spn
	swpc
	pop  ;|<-SAW
	push ;|
	swpc
	push
	call mid ; mid -> stack
	swpb
	pop  ;|<-SAW
	push ;|
	swpb
	push ; start -> stack
	call mergesort

	swpc
	push
	swpb
	add 1
	push
	call mergesort

	swpb
	push
	swpc
	push
	swpb
	push
	call merge

	;TODO del args
	.end:
ret

i:
resw 1
j:
resw 1
k:
resw 1
midplusone:
resw 1
rightsubleftplusone:
resw 1
left:
resw 1
right:
resw 1
merge:;(stack: dat mid+1, dat left, dat right)
	;brp ;168
	pop
	mov [midplusone]
	mov [j]
	pop
	mov [left]
	mov [i]
	pop
	mov [right]
	;(right - left) + 1
	sub [left]
	add 1
	mov [rightsubleftplusone]
	
	ldm 0
	mov [k]

	.while:
		;brp ;216
		ldm [k]
		sub [rightsubleftplusone]
		jez ..end

		ldm [midplusone]
		sub [i]
		jez ..L4

		ldm [right]
		add 1
		sub [j]
		jez ..L3
		
		ldm [j]
		mul 2
		add arr
		push
		ldm [i]
		mul 2
		add arr
		push
		;brp ; 286 debug
		ldm [#spn]
		popnil
		swpc
		ldm [#spn]
		popnil
		push
		swpc
		sub #spn
		popnil
		jgz ..L4 ; <=
		..L3:
			;b[k] = arr[i]   # sel.first
			ldm [k]
			mul 2
			add barr
			push
			
			ldm [i]
			mul 2
			add arr
			push
			ldm [#spn]
			popnil

			mov [#spn]
			popnil

			;i += 1
			ldm [i]
			add 1
			mov [i]
			jmp ..Lend
		..L4:
			;b[k] = arr[j]   # sel.second
			ldm [k]
			mul 2
			add barr
			push
			
			ldm [j]
			mul 2
			add arr
			push
			ldm [#spn]
			popnil

			mov [#spn]
			popnil

			;j += 1
			ldm [j]
			add 1
			mov [j]
		..Lend:

		ldm [k]
		add 1
		mov [k]

		jmp .while
	..end:

	ldm 0
	mov [k]

	.while2:
		;brp ;448
		ldm [k]
		sub [rightsubleftplusone]
		jez .while2.end

		ldm [left]
		add [k]
		mul 2
		add arr
		push

		ldm [k]
		mul 2
		add barr
		push
		ldm [#spn]
		popnil

		mov [#spn]
		popnil

		ldm [k]
		add 1
		mov [k]

		jmp .while2
	..end:
	;while k < (end - start) + 1:
	;	arr[start + k] = b[k]
	;	k += 1

ret


mid:;(stack: dat a, dat b); ret: (a+b)/2
	pop
	add #spn
	popnil
	div 2
	push
ret

print_arr:;(stack: dat left, dat right)
	ldm 9999
	mov #out
	pop
	mul 2
	add arr
	.loop:
		push
		ldm [#spn]
		mov #out
		pop
		add 2
		push
		swpc
		pop
		sub #spn
		sub #spn
		sub arr
		;add 2
		jgz .end
		swpc
		jmp .loop
	.end:
	popnil ;for del right
	ldm 9999
	mov #out
ret


arr:
resw 1000
setz
barr:
resw 1000
