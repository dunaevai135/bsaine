#include <iostream>
#include <string>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cstring>

#include "instructions.cpp"

using namespace std;

int runProgramm(int8_t *, int16_t *);

int main(int argc, char const *argv[]) {
/**/
	string fileDirection(argv[1]);
/*/
	string fileDirection;
	getline(cin, fileDirection);
/**/

	int fd = open(fileDirection.c_str(), O_RDWR);
	if(fd < 0) {
		std::cerr << "fileMappingCreate - open failed, file =  "
		          << fileDirection.c_str() << ", " << strerror(errno) << std::endl;
		return 0;
	}
	struct stat st{};
	if(fstat(fd, &st) < 0) {
		std::cerr << "fileMappingCreate - fstat failed, fname = "
		          << fileDirection.c_str() << ", " << strerror(errno) << std::endl;
		close(fd);
		return 0;
	}

	size_t fsize = (size_t)st.st_size;
	

	unsigned char* dataPtr = (unsigned char*)mmap(NULL, fsize, PROT_READ, MAP_SHARED,
	                                              fd, 0);
	if(dataPtr == MAP_FAILED) {
		std::cerr << "fileMappingCreate - mmap failed, fname = "
		          << fileDirection.c_str() << ", " << strerror(errno) << std::endl;
		close(fd);
		return 0;
	}

	int32_t sectionsCount = 0;
	int32_t symbolsCount = 0;
	int32_t blobsCount = 0;
	int32_t codeSize = 0;
	int32_t stackSize = 0;

	sectionsCount = *(int16_t*)(dataPtr+0x20);
	symbolsCount = *(int16_t*)(dataPtr+0x24);
	blobsCount = *(int16_t*)(dataPtr+0x34);
	codeSize = *(int16_t*)(dataPtr+sectionsCount*24+symbolsCount*16+2*4+60);
	stackSize = *(int16_t*)(dataPtr+sectionsCount*24+symbolsCount*16+3*4+60);
//	cout << sectionsCount << " " << symbolsCount << " " << blobsCount << " " << codeSize << endl;

	int16_t blobe3Offset = sectionsCount*24+symbolsCount*16+blobsCount*4 + 60; //TODO may be not true
	int16_t blobe4Offset = sectionsCount*24+symbolsCount*16+blobsCount*4 + codeSize + 60;
	codePtr = (int8_t*)malloc(codeSize);
	memcpy(codePtr, (dataPtr+blobe3Offset), codeSize);

	userStack = new vector<int16_t> (dataPtr+blobe4Offset, dataPtr+blobe4Offset+stackSize);

	initarray();

	instructionPointer = 0;

	runProgramm(codePtr, &instructionPointer);

	free(codePtr);
	munmap(dataPtr, fsize);
	close(fd);
	return 0;
}

int runProgramm(int8_t *codPtr, int16_t *ip) {
	int16_t *cp = (int16_t*)codPtr;
	while(*(cp+*ip) != 0) { //hlt

		if(instr[*(cp+*ip)] != 0) {
			instr[*(cp+*ip)]();
		}
		(*ip)++;
		syncReg();
	}
	return 0;
}
