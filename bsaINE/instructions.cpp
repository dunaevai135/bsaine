//
// Created by dunaev on 17.12.17.
//
#ifndef BSAVMMMAP_INSTRUCTIONS_CPP
#define BSAVMMMAP_INSTRUCTIONS_CPP

#define NUM_OF_REGS 7


#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

void (*instr[128])();

vector<int16_t> callStack;

vector<int16_t> *userStack;

int8_t* codePtr;
int16_t instructionPointer;

/*reg's*/
int16_t reg[NUM_OF_REGS];
enum regs {
	ACC = 0b00000000,
	BCC = 0b00000001,
	BAC = 0b00000010,
	ERR = 0b00000011,
	IN  = 0b00000100,
	OUT = 0b00000101,
	SPN = 0b00000110,
};

void m_nop() {
	cerr << " m_nop" <<endl;
}
void m_hlt() {
	cerr << " m_hlt" <<endl;
	cout << reg[ERR] << endl;
}
void m_brp() {
	cerr << " m_brp" <<endl;
	cerr << "brpeak p "<< instructionPointer*2 << endl;
//	getchar();
}
void m_swpb() {
	cerr << " m_swpb" <<endl;
	int16_t tmp;
	tmp=reg[ACC];
	reg[ACC]=reg[BAC];
	reg[BAC]=tmp;
}
void m_swpc() {
	cerr << " m_swpc" <<endl;
	int16_t tmp;
	tmp=reg[ACC];
	reg[ACC]=reg[BCC];
	reg[BCC]=tmp;
}
void m_movd() {
	cerr << " m_movd" <<endl;
//	*(codePtr + ++(instructionPointer)) = reg[ACC]; //TODO wtf
	cout << "wtf" << endl;
}
void m_movo() {
	cerr << " m_movo" <<endl;
	++(instructionPointer);
	*(int16_t*)(codePtr + (*(int16_t*)(codePtr + (instructionPointer)*2))) = reg[ACC];
}
void m_movro() {
	cerr << " m_movro" <<endl;
	++(instructionPointer);
	*(int16_t*)(codePtr + (reg[*(codePtr + (instructionPointer)*2)])) = reg[ACC];//TODO think about it "/2"
}
void m_movr() { //TODO IO
	cerr << " m_movr" <<endl;
	++(instructionPointer);
	if(*(int16_t*)(codePtr + (instructionPointer)*2) == OUT) {
		cout << reg[ACC] << endl;
	}
	reg[*(int16_t*)(codePtr + (instructionPointer)*2)] = reg[ACC];
}
void m_ldmd() {
	cerr << " m_ldmd" <<endl;
	++(instructionPointer);
	reg[ACC] = *(int16_t*)(codePtr + (instructionPointer)*2);
}
void m_ldmo() {
	cerr << " m_ldmo" <<endl;
	++(instructionPointer);
	reg[ACC] = *(int16_t*)(codePtr + (*(int16_t*)(codePtr + (instructionPointer)*2)));
}
void m_ldmro() {//IO
	cerr << " m_ldmro" <<endl;
	++(instructionPointer);
	reg[ACC] = *(int16_t*)(codePtr + (reg[*(int16_t*)(codePtr + (instructionPointer)*2)]));//TODO XXX
}
void m_ldmr() {//IO
	cerr << " m_ldmr" <<endl;
	++(instructionPointer);
	if(*(int16_t*)(codePtr + (instructionPointer)*2) == IN) {
		cin >> reg[IN];
	}
	reg[ACC] = reg[*(int16_t*)(codePtr + (instructionPointer)*2)];
}
void m_addd() {
	cerr << " m_addd" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmd();
	reg[ACC] += tmp;
}
void m_addo() {
	cerr << " m_addo" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmo();
	reg[ACC] += tmp;
}
void m_addr() {
	cerr << " m_addr" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmr();
	reg[ACC] += tmp;
}
void m_subd() {
	cerr << " m_subd" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmd();
	reg[ACC] = tmp - reg[ACC];
}
void m_subo() {
	cerr << " m_subo" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmo();
	reg[ACC] = tmp - reg[ACC];
}
void m_subr() {
	cerr << " m_subr" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmr();
	reg[ACC] = tmp - reg[ACC];
}
void m_muld() {
	cerr << " m_muld" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmd();
	reg[ACC] *= tmp;
}
void m_mulo() {
	cerr << " m_mulo" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmo();
	reg[ACC] *= tmp;
}
void m_mulr() {
	cerr << " m_mulr" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmr();
	reg[ACC] *= tmp;
}
void m_divd() {
	cerr << " m_divd" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmd();
	reg[ACC] = tmp / reg[ACC];
}
void m_divo() {
	cerr << " m_divo" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmo();
	reg[ACC] = tmp / reg[ACC];
}
void m_divr() {
	cerr << " m_divr" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmr();
	reg[ACC] = tmp / reg[ACC];
}
void m_neg() {
	cerr << " m_neg" <<endl;
	reg[ACC] = -reg[ACC];
}
void m_ipush() { //TODO stack
	cerr << " m_ipush" <<endl;
	++instructionPointer;
	(*userStack).push_back(*(int16_t*)(codePtr + (instructionPointer)*2));
}
void m_mpush() {
	cerr << " m_mpush" <<endl;
	++(instructionPointer);
	(*userStack).push_back(*(int16_t*)(codePtr + (*(int16_t*)(codePtr + (instructionPointer)*2))));
}
void m_rpush() {
	cerr << " m_rpush" <<endl;
	++(instructionPointer);
	int16_t tmp;
	if(*(int16_t*)(codePtr + (instructionPointer)*2) == IN) {
		cin >> reg[IN];
	}
	(*userStack).push_back(reg[*(int16_t*)(codePtr + (instructionPointer)*2)]);
}
void m_apush() {
	cerr << " m_apush" <<endl;
	(*userStack).push_back(reg[ACC]);
}
void m_pop() {
	cerr << " m_pop" <<endl;
	reg[ACC] = (*userStack).back();
	(*userStack).pop_back();
}
void m_popnil() {
	cerr << " m_popnil" <<endl;
	(*userStack).pop_back();
}
void m_bord() {
	cerr << " m_bord" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmd();
	reg[ACC] |= tmp;
}
void m_boro() {
	cerr << " m_boro" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmo();
	reg[ACC] |= tmp;
}
void m_borr() {
	cerr << " m_borr" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmr();
	reg[ACC] |= tmp;
}
void m_bandd() {
	cerr << " m_bandd" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmd();
	reg[ACC] &= tmp;
}
void m_bando() {
	cerr << " m_bando" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmo();
	reg[ACC] &= tmp;
}
void m_bandr() {
	cerr << " m_bandr" <<endl;
	int16_t tmp = reg[ACC];
	m_ldmr();
	reg[ACC] &= tmp;
}
void m_jmp() {
	cerr << " m_jmp" <<endl;
	++(instructionPointer);
	instructionPointer = (*(int16_t*)(codePtr + (instructionPointer)*2) - 1)/2;
}
void m_jez() {
	cerr << " m_jez" <<endl;
	if(reg[ACC] == 0) {
		m_jmp();
	} else {
		++(instructionPointer);
	}
}
void m_jnz() {
	cerr << " m_jnz" <<endl;
	if(reg[ACC] != 0) {
		m_jmp();
	} else {
		++(instructionPointer);
	}
}
void m_jlz() {
	cerr << " m_jlz" <<endl;
	if(reg[ACC] < 0) {
		m_jmp();
	} else {
		++(instructionPointer);
	}
}
void m_jgz() {
	cerr << " m_jgz" <<endl;
	if(reg[ACC] > 0) {
		m_jmp();
	} else {
		++(instructionPointer);
	}
}
void m_call() { //TODO check
	cerr << " m_call" <<endl;
	callStack.push_back(instructionPointer);
	m_jmp();
	for (int i = 0; i < NUM_OF_REGS; ++i) {
		callStack.push_back(reg[i]);
	}
}
void m_ret() {
	cerr << " m_ret" <<endl;
	for (int i = NUM_OF_REGS-1; i >= 0; --i) {
		reg[i] = callStack.back();
		callStack.pop_back();
	}
	instructionPointer = (callStack.back())+1; //magic
	callStack.pop_back();
}

void m_setz() {
	memset(reg, 0, NUM_OF_REGS);
}

void syncReg() {
	reg[SPN] = userStack->back();
}

void initarray() {
	memset(instr, 0, 128);
	memset(reg, 0, NUM_OF_REGS);
	instr[0b00000001] = m_nop;
	instr[0b00000000] = m_hlt;
	instr[0b00000010] = m_brp;
	instr[0b01010001] = m_swpb;
	instr[0b01010010] = m_swpc;
	instr[0b00000011] = m_movd;
	instr[0b00000100] = m_movo;
	instr[0b00000101] = m_movr;
	instr[0b00001011] = m_ldmd;
	instr[0b00001100] = m_ldmo;
	instr[0b00001101] = m_ldmr;
	instr[0b00100000] = m_addd;
	instr[0b00100001] = m_addo;
	instr[0b00100010] = m_addr;
	instr[0b00100100] = m_subd;
	instr[0b00100101] = m_subo;
	instr[0b00100110] = m_subr;
	instr[0b00101000] = m_muld;
	instr[0b00101001] = m_mulo;
	instr[0b00101010] = m_mulr;
	instr[0b00101100] = m_divd;
	instr[0b00101101] = m_divo;
	instr[0b00101110] = m_divr;
	instr[0b00010010] = m_ipush;
	instr[0b00010011] = m_mpush;
	instr[0b00010100] = m_rpush;
	instr[0b00010110] = m_apush;
	instr[0b00010101] = m_pop;
	instr[0b00110001] = m_bord;
	instr[0b00110010] = m_boro;
	instr[0b00110011] = m_borr;
	instr[0b00110101] = m_bandd;
	instr[0b00110110] = m_bando;
	instr[0b00110111] = m_bandr;
	instr[0b01000100] = m_jmp;
	instr[0b01000101] = m_jez;
	instr[0b01000110] = m_jnz;
	instr[0b01001111] = m_jlz;
	instr[0b01001000] = m_jgz;
	instr[0b01000111] = m_call;
	instr[0b01001011] = m_ret;
	instr[0b00000111] = m_movro;
	instr[0b00001111] = m_ldmro;
	instr[0b00101111] = m_neg;
	instr[0b01001001] = m_setz;
	instr[0b00010111] = m_popnil;
}

#endif //BSAVMMMAP_INSTRUCTIONS_CPP
