//
// Created by dunaev on 17.12.17.
//

#ifndef BSAVMMMAP_INSTRUCTIONS_H
#define BSAVMMMAP_INSTRUCTIONS_H

void initarray();


void m_nop();
void m_hlt();
void m_brp();
void m_swpb();
void m_swpc();
void m_movd();
void m_movo();
void m_movr();
void m_ldmd();
void m_ldmo();
void m_ldmr();
void m_addd();
void m_addo();
void m_addr();
void m_subd();
void m_subo();
void m_subr();
void m_muld();
void m_mulo();
void m_mulr();
void m_divd();
void m_divo();
void m_divr();
void m_ipush();
void m_mpush();
void m_rpush();
void m_apush();
void m_pop();
void m_bord();
void m_boro();
void m_borr();
void m_bandd();
void m_bando();
void m_bandr();
void m_jmp();
void m_jez();
void m_jnz();
void m_jlz();
void m_jgz();
void m_call();
void m_ret();

#endif //BSAVMMMAP_INSTRUCTIONS_H
