[section __code]
__start:
	ldm #in
	mov [i]
	mul 2
	add arr
	push
	ldm 3
	mov [#spn]
	popnil

	ldm #in
	mov [j]
	mul 2
	add arr
	push
	ldm [#spn]
	popnil
	mov #out
	brp
	hlt
	
resw 1
i:
resw 1
j:
resw 1
arr:
resw 20
setz
