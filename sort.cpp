merge_sort:
  push rbp
  mov rbp, rsp
  sub rsp, 80
  mov QWORD PTR [rbp-56], rdi
  mov QWORD PTR [rbp-64], rsi
  mov DWORD PTR [rbp-68], edx
  mov DWORD PTR [rbp-72], ecx
  mov eax, DWORD PTR [rbp-68]
  cmp eax, DWORD PTR [rbp-72]
  jne .L2
  mov eax, DWORD PTR [rbp-68]
  lea rdx, [0+rax*4]
  mov rax, QWORD PTR [rbp-56]
  add rax, rdx
  mov edx, DWORD PTR [rbp-68]
  lea rcx, [0+rdx*4]
  mov rdx, QWORD PTR [rbp-64]
  add rdx, rcx
  mov eax, DWORD PTR [rax]
  mov DWORD PTR [rdx], eax
  mov rax, QWORD PTR [rbp-64]
  jmp .L3
.L2:
  mov edx, DWORD PTR [rbp-68]
  mov eax, DWORD PTR [rbp-72]
  add eax, edx
  shr eax
  mov DWORD PTR [rbp-16], eax
  mov ecx, DWORD PTR [rbp-16]
  mov edx, DWORD PTR [rbp-68]
  mov rsi, QWORD PTR [rbp-64]
  mov rax, QWORD PTR [rbp-56]
  mov rdi, rax
  call merge_sort
  mov QWORD PTR [rbp-24], rax
  mov eax, DWORD PTR [rbp-16]
  lea edi, [rax+1]
  mov edx, DWORD PTR [rbp-72]
  mov rsi, QWORD PTR [rbp-64]
  mov rax, QWORD PTR [rbp-56]
  mov ecx, edx
  mov edx, edi
  mov rdi, rax
  call merge_sort
  mov QWORD PTR [rbp-32], rax
  mov rax, QWORD PTR [rbp-24]
  cmp rax, QWORD PTR [rbp-56]
  jne .L4
  mov rax, QWORD PTR [rbp-64]
  jmp .L5
.L4:
  mov rax, QWORD PTR [rbp-56]
.L5:
  mov QWORD PTR [rbp-40], rax
  mov eax, DWORD PTR [rbp-72]
  sub eax, DWORD PTR [rbp-68]
  mov DWORD PTR [rbp-44], eax
  mov eax, DWORD PTR [rbp-68]
  mov DWORD PTR [rbp-4], eax
  mov eax, DWORD PTR [rbp-16]
  add eax, 1
  mov DWORD PTR [rbp-8], eax
  mov eax, DWORD PTR [rbp-68]
  mov DWORD PTR [rbp-12], eax
  jmp .L6
.L12:
  mov eax, DWORD PTR [rbp-4]
  cmp eax, DWORD PTR [rbp-16]
  ja .L7
  mov eax, DWORD PTR [rbp-8]
  cmp eax, DWORD PTR [rbp-72]
  ja .L7
  mov eax, DWORD PTR [rbp-4]
  lea rdx, [0+rax*4]
  mov rax, QWORD PTR [rbp-24]
  add rax, rdx
  mov edx, DWORD PTR [rax]
  mov eax, DWORD PTR [rbp-8]
  lea rcx, [0+rax*4]
  mov rax, QWORD PTR [rbp-32]
  add rax, rcx
  mov eax, DWORD PTR [rax]
  cmp edx, eax
  jge .L8
  mov eax, DWORD PTR [rbp-4]
  lea rdx, [0+rax*4]
  mov rax, QWORD PTR [rbp-24]
  add rax, rdx
  mov edx, DWORD PTR [rbp-12]
  lea rcx, [0+rdx*4]
  mov rdx, QWORD PTR [rbp-40]
  add rdx, rcx
  mov eax, DWORD PTR [rax]
  mov DWORD PTR [rdx], eax
  add DWORD PTR [rbp-4], 1
  jmp .L10
.L8:
  mov eax, DWORD PTR [rbp-8]
  lea rdx, [0+rax*4]
  mov rax, QWORD PTR [rbp-32]
  add rax, rdx
  mov edx, DWORD PTR [rbp-12]
  lea rcx, [0+rdx*4]
  mov rdx, QWORD PTR [rbp-40]
  add rdx, rcx
  mov eax, DWORD PTR [rax]
  mov DWORD PTR [rdx], eax
  add DWORD PTR [rbp-8], 1
  jmp .L10
.L7:
  mov eax, DWORD PTR [rbp-4]
  cmp eax, DWORD PTR [rbp-16]
  ja .L11
  mov eax, DWORD PTR [rbp-4]
  lea rdx, [0+rax*4]
  mov rax, QWORD PTR [rbp-24]
  add rax, rdx
  mov edx, DWORD PTR [rbp-12]
  lea rcx, [0+rdx*4]
  mov rdx, QWORD PTR [rbp-40]
  add rdx, rcx
  mov eax, DWORD PTR [rax]
  mov DWORD PTR [rdx], eax
  add DWORD PTR [rbp-4], 1
  jmp .L10
.L11:
  mov eax, DWORD PTR [rbp-8]
  lea rdx, [0+rax*4]
  mov rax, QWORD PTR [rbp-32]
  add rax, rdx
  mov edx, DWORD PTR [rbp-12]
  lea rcx, [0+rdx*4]
  mov rdx, QWORD PTR [rbp-40]
  add rdx, rcx
  mov eax, DWORD PTR [rax]
  mov DWORD PTR [rdx], eax
  add DWORD PTR [rbp-8], 1
.L10:
  add DWORD PTR [rbp-12], 1
.L6:
  mov eax, DWORD PTR [rbp-12]
  cmp eax, DWORD PTR [rbp-72]
  jbe .L12
  mov rax, QWORD PTR [rbp-40]
.L3:
  leave
  ret