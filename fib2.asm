[section __code]
__start:
	ldm 0x1
	swpc
	add #in
	add 0x1
loop:
	sub 0x1
	jez e
	swpc
	swpb
	push
	swpb
	add #spn
	popnil
	push
	swpb
	sub #spn
	popnil
	neg
	swpb
	mov #out
	swpc
	jmp loop
e:
	swpc
;	mov #out
	hlt
